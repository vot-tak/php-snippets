<?php

/**
 * Function for creating multiple variants of text. "Send {text|post|message} with brackets and pipes and receive {random|unique} version of it" -> 6 versions of this text
 *
 *@param string $text Text formatted with brackets and pipes
 *@return string Clean synonymized text
 */

function simpsyn($text)
{
    return preg_replace_callback('|\{([^\}]+)\}|', 
			function ($matches) {$sym = explode('|', $matches[1]);$rnd = rand(0, count($sym)-1); return $sym[$rnd];},
			$text);
}